## Crawling Nepali news websites to generate a NLP dataset
With increase in deep learning practices in Natural Language Processing, the target 
is to develop a dataset that can be used to implement deep learning algorithm for the 
Nepali language. While, morphological and semantic features of the languages differ, it 
cannot be denied that a lot of data is one of the fundamental necessity for deep neural nets. 

With this in mind, the first goal is to leverage scrapy to capture following sites:

1. Kantipur News [Open](https://www.kantipurdaily.com)
    
      __Webpage Structure__  
            To Do:
                Write Here   
      __Crawling Technique__  
            To Do:
                Write Here
