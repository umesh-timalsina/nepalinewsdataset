# -*- coding: utf-8 -*-
import scrapy


class KantipurdailybusinessSpider(scrapy.Spider):
    name = 'KantipurDailyBusiness'
    allowed_domains = ['kantipurdaily.com']
    start_urls = ['https://www.kantipurdaily.com/business/2019/05/10']
    absolute_url = 'https://www.kantipurdaily.com'
    

    def __init__(self):
        self.num_articles = 0

    def parse(self, response):
        page_links = response.xpath("//article//h2/a/@href").extract()
        for page_link in page_links:   
            yield scrapy.Request('https://kantipurdaily.com' + page_link, callback=self.parse_content)
        pass
        
    def parse_content(self, response):
        title = response.xpath('//meta[@property="og:title"]/@content').extract()
        author = response.xpath('//meta[@name="web_author"]/@content').extract()
        dummy = response.xpath('//article')
        print(dummy.extract())
        content = response.xpath('//article//div[@class="description"]//*[self::p or self::div]/text()').extract()
        content = "".join(content)
        if "प्रतिक्रियापठाउनुहोस्" in content:
            content = content[:content.index("प्रतिक्रियापठाउनुहोस्")-1]
        if "ADVER" in content:
            content = content[:content.index("ADVER")]
        yield {
            'title' : title,
            'author' : author,
            'content' : content,
            'category' : 'business',

        }